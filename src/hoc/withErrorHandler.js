import React, { Component } from 'react';
import Aux from '../hoc/Auxi';
import Modal from '../components/UI/Modal/Modal';
const withErrorHandler = (WrappedComponent, axios) => {
    return class extends Component {

        state = {
            error: null
        }

        componentWillMount(){
            this.reqIntercepters =  axios.interceptors.request.use(req => {
                this.setState({error: null});
                return req;
            });
            this.resIntercepters =  axios.interceptors.response.use(null, error => {
                this.setState({error: error});
                return error;
            });
        }

        componentWillUnmount(){
            axios.interceptors.request.eject(this.reqIntercepters);
            axios.interceptors.response.eject(this.resIntercepters);
        }

        errorConfirmedHandler = () => {
            console.log("called");
            this.setState({error: null})
        }

        render() {
            return(
                <Aux>
                    <Modal
                        show={this.state.error}
                        cancelBackdrop={this.errorConfirmedHandler}>
                        <p style={{textAlign: 'center'}}>
                            {this.state.error ? this.state.error.message : null}
                        </p>
                    </Modal>
                    <WrappedComponent {...this.props} />
                </Aux>
            );
        }
    }
}
export default withErrorHandler;