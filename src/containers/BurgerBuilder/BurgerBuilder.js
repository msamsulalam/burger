import React, {Component} from 'react';
import Aux from '../../hoc/Auxi';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Ordersummary/Ordersummary';
import Spinner from '../../components/UI/Modal/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler';

import axios from '../../axios-orders';

const INGREDIENTS_BASE_PRICE = {
    meat: 0.5,
    cheese: 0.1,
    salad: 0.3,
    bacon: 0.4,
};

class BurgerBuilder extends Component {

    state = {
        ingredients: null,
        totalPrice: 0,
        purchasable: false,
        purchasing: false,
        loading: false,
        error: false,
    };

    componentDidMount() {
        axios.get("https://react-burger-app-2f2da.firebaseio.com/ingredients.json")
            .then( res => {
                this.setState({ingredients: res.data});
                this.updatePurchaseState(this.state.ingredients);
                let totalPrice = 0;
                Object.keys(this.state.ingredients).map( igkey =>{
                    const basePrice = INGREDIENTS_BASE_PRICE[igkey];
                    const totalItemPrice = basePrice * this.state.ingredients[igkey];
                    totalPrice = totalPrice + totalItemPrice;
                    this.setState({totalPrice: totalPrice});
                    return totalPrice;
                });
            }).catch( e => this.setState({error: true}));
    }

    addIngredientHandler = (type) => {
        const oldCount = this.state.ingredients[type];
        const updatedCount = oldCount + 1;
        const updatedIngredients = {...this.state.ingredients};
        updatedIngredients[type] = updatedCount;
        const priceAddition = INGREDIENTS_BASE_PRICE[type];
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice + priceAddition;
        this.setState({totalPrice: newPrice, ingredients: updatedIngredients});
        this.updatePurchaseState(updatedIngredients);
    };

    removeIngredientHandler = (type) => {
        const oldCount = this.state.ingredients[type];
        if (oldCount > 0) {
            const updatedCount = oldCount - 1;
            const updatedIngredients = {...this.state.ingredients};
            updatedIngredients[type] = updatedCount;
            const priceAddition = INGREDIENTS_BASE_PRICE[type];
            const oldPrice = this.state.totalPrice;
            const newPrice = oldPrice - priceAddition;
            this.setState({totalPrice: newPrice, ingredients: updatedIngredients});
            this.updatePurchaseState(updatedIngredients);
        }
    };

    updatePurchaseState(ingredients) {
        const sum = Object.keys(ingredients)
            .map(igkey => {
                return ingredients[igkey];
            }).reduce((sum, el) => {
                return sum + el;
            }, 0);
        this.setState({purchasable: sum > 0});
    }

    purchaseHandler = () => {
        this.setState({purchasing: true});
    }

    cancelHandler = () => {
        this.setState({purchasing: false});
    }

    purchasecancelHandler = () => {
        this.setState({purchasing: false});
    }

    purchaseContinueHandler = () => {
        this.setState({loading: true, purchasing: false});
        const orders = {
            ingredients: this.state.ingredients,
            price: this.state.totalPrice,
            customerInfo: {
                name: "Md. Samsul Alam",
                phone: "01778553626",
                address: {
                    street: "Nikunja-02",
                    postalCode: "1229"
                }
            }
        }
        axios.post("orders.json", orders)
            .then(response => this.setState({loading: false, purchasing: false}))
            .catch(error => this.setState({loading: false, purchasing: false}));
    }

    render() {
        const disabledInfo = {
            ...this.state.ingredients
        };
        for (let key in disabledInfo) {
            disabledInfo[key] = disabledInfo[key] <= 0
        }

        let orderSummary = null;

        if (this.state.loading) {
            orderSummary = <Spinner/>;
        }

        let burger = this.state.errorr ? <p>Ingredients can't be loaded</p> : <Spinner/>;
        if (this.state.ingredients) {
            burger = (
                <Aux>
                    <Burger ingredients={this.state.ingredients}/>
                    <BuildControls
                        ordered={this.purchaseHandler}
                        addedIngredients={this.addIngredientHandler}
                        removeIngredients={this.removeIngredientHandler}
                        disabled={disabledInfo}
                        price={this.state.totalPrice}
                        purchasable={this.state.purchasable}
                    />
                </Aux>
            );
            orderSummary = <OrderSummary
                price={this.state.totalPrice}
                ingredients={this.state.ingredients}
                purchasecancelHandler={this.purchasecancelHandler}
                purchaseContinueHandler={this.purchaseContinueHandler}
            />;
        }

        return (
            <Aux>
                <Modal show={this.state.purchasing} cancelBackdrop={this.cancelHandler}>
                    {orderSummary}
                </Modal>
                {burger}
            </Aux>
        );
    }
}

export default withErrorHandler(BurgerBuilder, axios);