import React, {Component} from 'react';
import CheckoutSummary from '../../components/UI/CheckoutSummary/CheckoutSummary';
class Checkout extends Component {

    state = {
        ingredients:{
            salad: 2,
            cheese:1,
            meat: 3,
            bacon:1
        }
    }

    render() {
        return (
            <CheckoutSummary ingredients={this.state.ingredients} />
        );
    }
}

export default Checkout;

