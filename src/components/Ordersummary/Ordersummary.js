import React, { Component } from 'react';
// import classes from '../Ordersummary/Ordersummary.module.css';
import Aux from '../../hoc/Auxi';
import Button from '../Button/Button';

class Ordersummary extends Component {
    componentWillUpdate(){
        console.log('[Ordersummary Update]');
    }
    render(){
        const ordersummary = Object.keys(this.props.ingredients)
            .map((igKey) =>{
                return <li key={igKey}><span style={{textTransform: 'capitalize'}}>{igKey}</span>: {this.props.ingredients[igKey]}</li>
            });
        return(
            <Aux>
                <h3>Your Order</h3>
                <p>A delicious burger with the following ingredients:</p>
                <ul>
                    {ordersummary}
                </ul>
                <p><strong>Total Price: {this.props.price.toFixed(2)}</strong></p>
                <Button btnType="Danger" clicked={this.props.purchasecancelHandler}>CANCEL</Button>
                <Button btnType="Success" clicked={this.props.purchaseContinueHandler}>Continue</Button>
            </Aux>
        );
    }
}
export default Ordersummary;

// const ordersummary =(props)=>{
//     const ordersummary = Object.keys(props.ingredients)
//         .map((igKey) =>{
//             return <li key={igKey}><span style={{textTransform: 'capitalize'}}>{igKey}</span>: {props.ingredients[igKey]}</li>
//         });
//     return(
//         <Aux>
//             <h3>Your Order</h3>
//             <p>A delicious burger with the following ingredients:</p>
//             <ul>
//                 {ordersummary}
//             </ul>
//             <p><strong>Total Price: {props.price.toFixed(2)}</strong></p>
//             <Button btnType="Danger" clicked={props.purchasecancelHandler}>CANCEL</Button>
//             <Button btnType="Success" clicked={props.purchaseContinueHandler}>Continue</Button>
//         </Aux>
//     );
// };
//
// export default ordersummary;
