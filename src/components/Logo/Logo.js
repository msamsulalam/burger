import React from 'react';
import classess from '../Logo/Logo.module.css';
import burgerLogo from '../../assets/images/27.1 burger-logo.png.png';
const logo = (props) =>{
    return (
        <div className={classess.Logo}>
            <img src={burgerLogo} alt="MyBurger"/>
        </div>
    );
};

export default logo;