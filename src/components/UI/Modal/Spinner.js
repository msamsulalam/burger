import  React from 'react';
import classess from '../Modal/Spinner.module.css';
const spinner = () => {
    return (
        <div className={classess.Loader}>Loading...</div>
    );
}
export default spinner;