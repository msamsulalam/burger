import React from 'react';

import Burger from '../../Burger/Burger';
import Button from '../../Button/Button';

import Classess from './CheckoutSummary.css';

const CheckoutSummary = (props) => {
    return (
        <div className={Classess.CheckoutSummary}>
            <div style={{width: '100%', height: '300px', margin: 'auto'}}>
                <h1>We Hope It Will Taste Well!</h1>
                <Burger ingredients={props.ingredients}/>
                <Button btnType="Danger" clicked> CANCEL </Button>
                <Button btnType="Success" clicked> CONTINUE </Button>
            </div>
        </div>
    );
}

export default CheckoutSummary;