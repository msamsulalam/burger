import React from 'react';
import classess from './BuildControl.module.css';

const buildControl = (props) => {
    return (
        <div className={classess.BuildControl}>
            <div>{props.label}</div>
            <button className={classess.More} onClick={props.added}>More</button>
            <button className={classess.Less} onClick={props.remove} disabled={props.disabled}>Less</button>
        </div>
    );
};

export default buildControl;