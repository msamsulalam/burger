import React, { Component } from 'react';
import Aux from '../../hoc/Auxi';
import classes from './Layout.module.css';
import Toolbar from '../Navigation/Toolbar/Toolbar';
import SideDrawer from '../Navigation/SideDrawer/SideDrawer';

class Layout extends Component {

    state ={
        showSideDrawer : false
    }
    sideDrawerClosedHandler = () =>{
        this.setState({showSideDrawer: false});
    };

    sideDrawerToogleHandler = () =>{
        this.setState({showSideDrawer: !this.state.showSideDrawer});
    };

    render(){
        return(
            <Aux>
                <Toolbar clicked={this.sideDrawerToogleHandler}/>
                <SideDrawer open={this.state.showSideDrawer} closed={this.sideDrawerClosedHandler} />
                <div>SideDrawer, Backdrop</div>
                <main className={classes.Content}>
                    {this.props.children}
                </main>
            </Aux>
        )
    }
}

export default Layout;