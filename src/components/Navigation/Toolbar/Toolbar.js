import React from 'react';
import classess from './Toolbar.module.css';
import Logo from '../../Logo/Logo';
import NavigationItem from '../NavigationItems/NavigationItems';
import DrawerToggle from '../SideDrawer/DrawerToggle/DrawerToggle';
const toolbar = (props) =>{
    return (
        <header className={classess.Toolbar}>
            <DrawerToggle clicked={props.clicked} />
            <div className={classess.Logo}>
                <Logo />
            </div>
            <nav className={classess.DesktopOnly}>
                <NavigationItem></NavigationItem>
            </nav>
        </header>
    );
};

export default toolbar;