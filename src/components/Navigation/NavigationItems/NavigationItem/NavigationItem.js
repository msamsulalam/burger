import React from 'react';
import classess from '../NavigationItem/NavigationItem.module.css';
const navigationItems = (props) =>{
    return (
        <li className={classess.NavigationItem}>
            <a
                href={props.link}
                className={props.active ? classess.active : null}>
                {props.children}
            </a>
        </li>
    );
};

export default navigationItems;