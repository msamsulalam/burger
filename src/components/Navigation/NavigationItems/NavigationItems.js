import React from 'react';
import classess from '../NavigationItems/NavigationItems.module.css';
import NavigationItem from './NavigationItem/NavigationItem';
const navigationItems = (props) =>{
  return (
      <ul className={classess.NavigationItems}>
          <NavigationItem link="/" active >Burger Builder</NavigationItem>
          <NavigationItem link="/" >Checkout</NavigationItem>
      </ul>
  );
};

export default navigationItems;